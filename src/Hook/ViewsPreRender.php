<?php

namespace Drupal\synfilters\Hook;

/**
 * @file
 * Contains \Drupal\synfilters\Hook\ViewsPreRender.
 */

/**
 * Theme.
 */
class ViewsPreRender {

  /**
   * Hook.
   */
  public static function hook($view) {
    if ($view->id() == 'product' && $view->current_display == 'embed') {
      $current_path = \Drupal::service('path.current')->getPath();
      $result = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
      $view->exposed_widgets['#action'] = $result;
    }
  }

}
