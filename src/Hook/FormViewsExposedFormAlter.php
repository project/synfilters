<?php

namespace Drupal\synfilters\Hook;

/**
 * @file
 * Contains \Drupal\synfilters\Hook\FormViewsExposedFormAlter.
 */

/**
 * Theme.
 */
class FormViewsExposedFormAlter {

  /**
   * Hook.
   */
  public static function hook(&$form, $form_state, $form_id) {
    if ($form['#id'] == 'views-exposed-form-product-embed') {
      $term = \Drupal::routeMatch()->getParameter('taxonomy_term');
      if (is_object($term)) {
        $child_tids = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($term->bundle(), $term->id());
        if (!empty($child_tids)) {
          $form['#prefix'] = '<div class="hidden">';
          $form['#suffix'] = '</div>';
        }
      }
    }
  }

}
