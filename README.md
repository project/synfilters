# _synfilters_

## Чем занимается?

> Добавляет в конфигурацию представления views.view.product
> зависимость от taxonomy.vocabulary.product_options в раздел dependencies.config
> зависмость от taxonomy и better_exposed_filters в раздел dependencies.module
> Скрывает раскрытую форму на отображении продукта embed для родительских терминов

## Требования к платформе

- _Drupal 8-10_
- _PHP 7.4.0+_

## Версии

- [Drupal.org prod версия](https://www.drupal.org/project/synfilters)

```sh
composer require 'drupal/synfilters'
```

- [Drupal.org dev версия](https://www.drupal.org/project/synfilters/releases/8.x-1.x-dev)

```sh
composer require 'drupal/synfilters:1.x-dev@dev'
```

## Как использовать?

- Дополнительных настроек не требуется
